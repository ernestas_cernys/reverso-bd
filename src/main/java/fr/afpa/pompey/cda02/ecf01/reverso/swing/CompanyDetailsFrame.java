package fr.afpa.pompey.cda02.ecf01.reverso.swing;


import java.util.logging.Level;
import java.util.logging.Logger;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import fr.afpa.pompey.cda02.ecf01.reverso.Reverso;
import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;


/**
 * A frame holding a Form for Company-like objects.
 * 
 * Currently manages Client and Prospect classes.
 */
public class CompanyDetailsFrame extends javax.swing.JFrame {
    private JFrame parentFrame;
    private Action action;
    private Company company;
    
    
    /**
     * Construct a Frame containing a Company form.
     * 
     * If the action is set to CREATE, the `company` provided values will be 
     * ignored but its Class will determine the target type.
     * 
     * Conversely, a concrete Company must be provided if the action is 
     * UPDATE or DELETE.
     * 
     * The DELETE action will ensure that the form is readonly.
     * 
     * @param parentFrame
     * @param action
     * @param company
     * @throws ReversoException 
     */
    public CompanyDetailsFrame(
            Action action, 
            Company company
    ) throws ReversoException {
        
        super();
        
        this.initComponents();
        
        // Populate the domain ComboBox
        this.domainComboBox.removeAllItems();
        for (Company.Domain domain: Company.Domain.values()) {
            this.domainComboBox.addItem(domain);
        }
        this.domainComboBox.setRenderer(
            (jlist, value, index, isSelected, hasFocus) -> {
                return new JLabel(((Company.Domain)value).getRepresentation());
            });
        
        // Populate the interested ComboBox
        this.interestedComboBox.removeAllItems();
        this.interestedComboBox.addItem(true);
        this.interestedComboBox.addItem(false);
        this.interestedComboBox.setRenderer(
            (jlist, value, index, isSelected, hasFocus) -> {
                Boolean booleanValue = (Boolean) value;
                return new JLabel(booleanValue ? "Oui": "Non");
            });
        
        // Turn invisible both client and prospect specific panels invisible
        // by default.
        this.clientFieldsPanel.setVisible(false);
        this.prospectFieldsPanel.setVisible(false);
        
        // All visual components are reading, now validate and load the values
        this.parentFrame = parentFrame;
        this.setAction(action);
        this.setCompany(company);
    }
    
    public Action getAction() {
        return action;
    }

    /**
     * Validate the company form and return the corresponding object
     * on success, throw a ReversoDisplayableException otherwise.
     * 
     * @return
     * @throws ReversoDisplayableException 
     */
    public Company getCompany() throws ReversoDisplayableException {
        // Business Name
        try {
            company.setBusinessName(
                    this.businessNameTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "La raison sociale ne peut être vide"
            );
        }
        
        // Domain
        try {
            company.setDomain(
                (Company.Domain)this.domainComboBox.getSelectedItem());
        }
        catch (ReversoException re) {
            // This error should not occur has we always pass a value
            throw new ReversoDisplayableException(
                re,
                "Le domaine doit être renseigné"
            );
        }
        
        // Street Number
        try {
            company.setAddressStreetNumber(
                Integer.parseInt(this.addressStreetNumberTextField.getText()));
        }
        catch (NumberFormatException nfe) {
            throw new ReversoDisplayableException(
                nfe,
                "Le numéro de rue doit être renseigné"
            );
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "Le numéro de rue doit être positif"
            );
        }
        
        // Street name
        try {
            company.setAddressStreetName(
                this.addressStreetNameTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "Le nom de rue doit être renseigné"
            );
        }
        
        // Zip code
        try {
            company.setAddressZipCode(
                this.addressZipCodeTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "Le code postal ne peut pas être vide"
            );
        }
        
        // City
        try {
            company.setAddressCity(
                this.addressCityTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "La ville doit être renseignée"
            );
        }
        
        // Phone number
        try {
            company.setPhoneNumber(
                this.phoneNumberTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "Le numéro de téléphone ne peut pas être vide et doit " + 
                "respecter le format XX XX XX XX XX, avec ou sans indicatif"
            );
        }
        
        // Email
        try {
            company.setEmail(
                this.emailTextField.getText().trim());
        }
        catch (ReversoException re) {
            throw new ReversoDisplayableException(
                re,
                "L'email doit être renseigné et respecter le format " +
                "xxx.yyy.zzz@sub.exemple.com"
            );
        }
        
        
        // Comments
        company.setComments(this.commentsTextArea.getText().trim());
        
        if (company instanceof Client) {
            // Turnover
            try {
                ((Client) company).setTurnover(
                    Integer.parseInt(this.turnoverTextField.getText()));
            }
            catch (NumberFormatException nfe) {
                throw new ReversoDisplayableException(
                    nfe,
                    "Le chiffre d'affaire n'est pas un nombre valide"
                );
            }
            catch (ReversoException re) {
                throw new ReversoDisplayableException(
                    re,
                    "Le chiffre d'affaire doit être au moins dix fois supérieur"
                    + " à l'effectif"
                );
            }
            
            // Headcount
            try {
                ((Client) company).setHeadcount(
                    Integer.parseInt(this.headcountTextField.getText()));
            }
            catch (NumberFormatException nfe) {
                throw new ReversoDisplayableException(
                    nfe,
                    "L'effectif n'est pas un nombre valide"
                );
            }
            catch (ReversoException re) {
                throw new ReversoDisplayableException(
                    re,
                    "L'effectif doit être supérieur à zéro et au moins dix fois"
                    + " inférieur au chiffre d'affaire"
                );
            }
        }
        
        if (company instanceof Prospect) {
            // Prospection Date
            try {
                ((Prospect) company).setProspectionDate(
                    this.prospectionDateTextField.getText().trim());
            }
            catch (ReversoException re) {
                throw new ReversoDisplayableException(
                    re,
                    "La date de prospection doit être renseignée au format" 
                    + " JJ/MM/AAAA"
                );
            }
            
            // Interested
            ((Prospect) company).setInterested(
                (Boolean) this.interestedComboBox.getSelectedItem());
        }
        
        
        return company;
    }

    /**
     * `action` must be set.
     * If set to CREATE, the `company` attribute must be null.
     * If set to UPDATE or DELETE, the `company` attribute must be provided.
     * The LIST action is currently unsupported, you might want to look for 
     * th ListFrame component.
     * 
     * @param action
     * @throws ReversoException 
     */
    public void setAction(Action action) throws ReversoException {
        this.action = action;
        
        if (action == null) {
           throw new ReversoException("Action cannot be null");
        }
        
        switch (this.action) {
            case CREATE:
            case UPDATE:
                this.toggleElements(this.formPanel, true);
                this.toggleElements(this.idTextField, false);
                break;
                
            case DELETE:
                this.toggleElements(this.formPanel, false);
                break;
                
            default:
                throw new UnsupportedOperationException(
                        "Unsupported action " + this.action);
        }
    }
    
    /**
     * Set the company and populate the form accordingly.
     * Will check the parameter `action` to, potentially, throw a 
     * ReversoException.
     * 
     * @param company
     * @throws ReversoException 
     */
    public void setCompany(Company company) throws ReversoException {
        if (company == null) {
            throw new ReversoException(
                "No Company was provided");
        }
        
        if (company != null) {
            this.idTextField.setText(
                    Integer.toString(company.getId()));
            this.businessNameTextField.setText(
                    company.getBusinessName());
            this.domainComboBox.setSelectedItem(
                    company.getDomain());
            this.addressStreetNumberTextField.setText(
                    Integer.toString(company.getAddressStreetNumber()));
            this.addressStreetNameTextField.setText(
                    company.getAddressStreetName());
            this.addressZipCodeTextField.setText(
                    company.getAddressZipCode());
            this.addressCityTextField.setText(
                    company.getAddressCity());
            this.phoneNumberTextField.setText(
                    company.getPhoneNumber());
            this.emailTextField.setText(
                    company.getEmail());
            this.commentsTextArea.setText(
                    company.getComments());
            
            if (company instanceof Client) {
                this.clientFieldsPanel.setVisible(true);
                
                Client client = (Client) company;
                this.turnoverTextField.setText(
                        Integer.toString(client.getTurnover()));
                this.headcountTextField.setText(
                        Integer.toString(client.getHeadcount()));
            }
            else if (company instanceof Prospect) {
                this.prospectFieldsPanel.setVisible(true);
                
                Prospect prospect = (Prospect) company;
                this.prospectionDateTextField.setText(
                        prospect.getProspectionDate());
                this.interestedComboBox.setSelectedItem(
                        prospect.isInterested());
            }
        }
        
        this.company = company;
    }
    
    /**
     * Toggle the component and its children recursively
     * 
     * @param rootContainer
     * @param enabled 
     */
    private void toggleElements(Container rootContainer, boolean enabled) { 
        rootContainer.setEnabled(enabled);
        
        for (Component component: rootContainer.getComponents()) {
            if (component instanceof Container) {
                toggleElements((Container) component, enabled);
            }
            if (component instanceof JComponent 
                && !(component instanceof JLabel)) {
                
                ((JComponent) component).setEnabled(enabled);
            }
        }
    }
    
    
    /**
     * Let the system dispose of the current Frame
     */
    public void quit() {
        new HomeFrame().setVisible(true);
        this.dispose();
    }
    
    public static void main(String[] args) throws ReversoException {
        Reverso.loadTestData();
        Action action = Action.UPDATE;
        Company company = Company.getList().get(0);
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CompanyDetailsFrame app;
                try {
                    app = new CompanyDetailsFrame(action, company);
                    app.setVisible(true);
                    
                } catch (ReversoException ex) {
                    Logger.getLogger(CompanyDetailsFrame.class.getName())
                        .log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize 
     * the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    cancelButton = new javax.swing.JButton();
    confirmButton = new javax.swing.JButton();
    formPanel = new javax.swing.JPanel();
    idLabel = new javax.swing.JLabel();
    idTextField = new javax.swing.JTextField();
    businessNameLabel = new javax.swing.JLabel();
    businessNameTextField = new javax.swing.JTextField();
    domainLabel = new javax.swing.JLabel();
    domainComboBox = new javax.swing.JComboBox<>();
    addressStreetNumberLabel = new javax.swing.JLabel();
    addressStreetNumberTextField = new javax.swing.JTextField();
    addressStreetNameLabel = new javax.swing.JLabel();
    addressStreetNameTextField = new javax.swing.JTextField();
    addressZipCodeLabel = new javax.swing.JLabel();
    addressZipCodeTextField = new javax.swing.JTextField();
    addressCityLabel = new javax.swing.JLabel();
    addressCityTextField = new javax.swing.JTextField();
    phoneNumberLabel = new javax.swing.JLabel();
    phoneNumberTextField = new javax.swing.JTextField();
    emailLabel = new javax.swing.JLabel();
    emailTextField = new javax.swing.JTextField();
    commentsLabel = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    commentsTextArea = new javax.swing.JTextArea();
    clientFieldsPanel = new javax.swing.JPanel();
    turnoverLabel = new javax.swing.JLabel();
    turnoverTextField = new javax.swing.JTextField();
    heacountLabel = new javax.swing.JLabel();
    headcountTextField = new javax.swing.JTextField();
    prospectFieldsPanel = new javax.swing.JPanel();
    prospectionDateLabel = new javax.swing.JLabel();
    prospectionDateTextField = new javax.swing.JTextField();
    interestedLabel = new javax.swing.JLabel();
    interestedComboBox = new javax.swing.JComboBox<>();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    cancelButton.setText("Retour");
    cancelButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        cancelButtonActionPerformed(evt);
      }
    });

    confirmButton.setText("Valider");
    confirmButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        confirmButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(cancelButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(confirmButton)
        .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(confirmButton)
          .addComponent(cancelButton))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    formPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Détails"));

    idLabel.setText("Identifiant");

    businessNameLabel.setText("Raison Sociale");

    domainLabel.setText("Domaine");

    addressStreetNumberLabel.setText("Numéro de rue");

    addressStreetNameLabel.setText("Nom de rue");

    addressZipCodeLabel.setText("Code Postal");

    addressCityLabel.setText("Ville");

    phoneNumberLabel.setText("Numéro deTéléphone");
    phoneNumberLabel.setToolTipText("");

    emailLabel.setText("Adresse Mail");
    emailLabel.setToolTipText("");

    commentsLabel.setText("Commentaires");
    commentsLabel.setToolTipText("");

    commentsTextArea.setColumns(20);
    commentsTextArea.setRows(5);
    jScrollPane1.setViewportView(commentsTextArea);

    turnoverLabel.setText("Chiffre d'affaire");

    heacountLabel.setText("Effectif");

    javax.swing.GroupLayout clientFieldsPanelLayout = new javax.swing.GroupLayout(clientFieldsPanel);
    clientFieldsPanel.setLayout(clientFieldsPanelLayout);
    clientFieldsPanelLayout.setHorizontalGroup(
      clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, clientFieldsPanelLayout.createSequentialGroup()
        .addGap(51, 51, 51)
        .addGroup(clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(turnoverLabel, javax.swing.GroupLayout.Alignment.TRAILING)
          .addComponent(heacountLabel, javax.swing.GroupLayout.Alignment.TRAILING))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(turnoverTextField)
          .addComponent(headcountTextField))
        .addContainerGap())
    );
    clientFieldsPanelLayout.setVerticalGroup(
      clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(clientFieldsPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(turnoverTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(turnoverLabel))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(clientFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(headcountTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(heacountLabel))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    prospectionDateLabel.setText("Date de prospection");

    interestedLabel.setText("Est interessé");

    javax.swing.GroupLayout prospectFieldsPanelLayout = new javax.swing.GroupLayout(prospectFieldsPanel);
    prospectFieldsPanel.setLayout(prospectFieldsPanelLayout);
    prospectFieldsPanelLayout.setHorizontalGroup(
      prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, prospectFieldsPanelLayout.createSequentialGroup()
        .addGap(23, 23, 23)
        .addGroup(prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(prospectionDateLabel, javax.swing.GroupLayout.Alignment.TRAILING)
          .addComponent(interestedLabel, javax.swing.GroupLayout.Alignment.TRAILING))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(interestedComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(prospectionDateTextField))
        .addContainerGap())
    );
    prospectFieldsPanelLayout.setVerticalGroup(
      prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(prospectFieldsPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(prospectionDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(prospectionDateLabel))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(prospectFieldsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(interestedLabel)
          .addComponent(interestedComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(14, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout formPanelLayout = new javax.swing.GroupLayout(formPanel);
    formPanel.setLayout(formPanelLayout);
    formPanelLayout.setHorizontalGroup(
      formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(formPanelLayout.createSequentialGroup()
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(formPanelLayout.createSequentialGroup()
            .addGap(56, 56, 56)
            .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(domainLabel)
              .addComponent(businessNameLabel)
              .addComponent(idLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(domainComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(businessNameTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(idTextField, javax.swing.GroupLayout.Alignment.TRAILING)))
          .addGroup(formPanelLayout.createSequentialGroup()
            .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(formPanelLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                  .addComponent(addressCityLabel)
                  .addComponent(addressStreetNumberLabel)
                  .addComponent(addressStreetNameLabel)
                  .addComponent(addressZipCodeLabel)))
              .addGroup(formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(phoneNumberLabel))
              .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(emailLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                  .addComponent(commentsLabel, javax.swing.GroupLayout.Alignment.TRAILING))))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(addressStreetNumberTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(addressStreetNameTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
              .addComponent(phoneNumberTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(emailTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(addressCityTextField, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(addressZipCodeTextField, javax.swing.GroupLayout.Alignment.TRAILING))))
        .addContainerGap())
      .addComponent(clientFieldsPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
      .addComponent(prospectFieldsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    formPanelLayout.setVerticalGroup(
      formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(formPanelLayout.createSequentialGroup()
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(idTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(idLabel))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(businessNameLabel)
          .addComponent(businessNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(domainComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(domainLabel))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(addressStreetNumberLabel)
          .addComponent(addressStreetNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(addressStreetNameLabel)
          .addComponent(addressStreetNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(addressZipCodeLabel)
          .addComponent(addressZipCodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(addressCityLabel)
          .addComponent(addressCityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(phoneNumberLabel)
          .addComponent(phoneNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(emailLabel)
          .addComponent(emailTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(commentsLabel)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(clientFieldsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(prospectFieldsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(formPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(layout.createSequentialGroup()
            .addGap(240, 240, 240)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(formPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.quit();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void confirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmButtonActionPerformed
        try {
            Company company = this.getCompany();
            
            switch (this.getAction()) {
                case CREATE:
                    Company.getList().add(company);
                    break;
                case UPDATE:
                    // Nothing to do here, the form already updated
                    break;
                case DELETE:
                    Company.getList().remove(company);
                    break;
            }
            
            this.quit();
        }
        catch (ReversoDisplayableException rde) {
            JOptionPane.showMessageDialog(this, rde.getMessage());
        }
        

    }//GEN-LAST:event_confirmButtonActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel addressCityLabel;
  private javax.swing.JTextField addressCityTextField;
  private javax.swing.JLabel addressStreetNameLabel;
  private javax.swing.JTextField addressStreetNameTextField;
  private javax.swing.JLabel addressStreetNumberLabel;
  private javax.swing.JTextField addressStreetNumberTextField;
  private javax.swing.JLabel addressZipCodeLabel;
  private javax.swing.JTextField addressZipCodeTextField;
  private javax.swing.JLabel businessNameLabel;
  private javax.swing.JTextField businessNameTextField;
  private javax.swing.JButton cancelButton;
  private javax.swing.JPanel clientFieldsPanel;
  private javax.swing.JLabel commentsLabel;
  private javax.swing.JTextArea commentsTextArea;
  private javax.swing.JButton confirmButton;
  private javax.swing.JComboBox<Company.Domain> domainComboBox;
  private javax.swing.JLabel domainLabel;
  private javax.swing.JLabel emailLabel;
  private javax.swing.JTextField emailTextField;
  private javax.swing.JPanel formPanel;
  private javax.swing.JLabel heacountLabel;
  private javax.swing.JTextField headcountTextField;
  private javax.swing.JLabel idLabel;
  private javax.swing.JTextField idTextField;
  private javax.swing.JComboBox<Boolean> interestedComboBox;
  private javax.swing.JLabel interestedLabel;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JLabel phoneNumberLabel;
  private javax.swing.JTextField phoneNumberTextField;
  private javax.swing.JPanel prospectFieldsPanel;
  private javax.swing.JLabel prospectionDateLabel;
  private javax.swing.JTextField prospectionDateTextField;
  private javax.swing.JLabel turnoverLabel;
  private javax.swing.JTextField turnoverTextField;
  // End of variables declaration//GEN-END:variables
}
