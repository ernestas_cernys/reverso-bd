package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;


public class Prospect extends Company {
    private String prospectionDate;
    private boolean interested;
    
    /**
     * Creates an empty Prospect.
     */
    public Prospect() {
        super();
    }
    
    /**
     * Every value is required except for the comments
     * 
     * @param businessName
     * @param domain
     * @param addressStreetNumber
     * @param addressStreetName
     * @param addressZipCode
     * @param addressCity
     * @param phoneNumber
     * @param email
     * @param comments
     * @param prospectionDate
     * @param interested
     * @throws ReversoException 
     */
    public Prospect(
        final String businessName, 
        final Domain domain, 
        final int addressStreetNumber, 
        final String addressStreetName, 
        final String addressZipCode, 
        final String addressCity, 
        final String phoneNumber, 
        final String email, 
        final String comments,
        final String prospectionDate,
        final boolean interested
    ) throws ReversoException 
    {
        super(
            businessName,
            domain,
            addressStreetNumber,
            addressStreetName,
            addressZipCode,
            addressCity,
            phoneNumber,
            email,
            comments
        );
        
        this.setProspectionDate(prospectionDate);
        this.setInterested(interested);
    }

    public String getProspectionDate() {
        return prospectionDate;
    }

    public boolean isInterested() {
        return interested;
    }
    
    /**
     * The prospection date must be set and respect 
     * the DD/MM/YYYY format.
     * 
     * @param prospectionDate
     * @throws ReversoException 
     */
    public void setProspectionDate(String prospectionDate) 
        throws ReversoException
    {
        if (prospectionDate == null || prospectionDate.trim().isEmpty()) {
            throw new ReversoException(
                "Prospection date cannot be empty (received '" + prospectionDate + "'"
            );
        }
        if (!prospectionDate.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")) {
            throw new ReversoException(
                "The date format seems invalid (received '" + prospectionDate 
                        + "', expected something of the form DD-MM-YYYY)"
            );
        }

        
        this.prospectionDate = prospectionDate;
    }

    public void setInterested(boolean interested) {
        this.interested = interested;
    }
    
    @Override
    public String toString() {
        return "Prospect{" 
                + "id=" + this.getId() 
                + ", businessName=" + this.getBusinessName() 
                + ", domain=" + this.getDomain()
                + ", addressStreetNumber=" + this.getAddressStreetNumber()
                + ", addressStreetName=" + this.getAddressZipCode()
                + ", addressZipCode=" + this.getAddressZipCode()
                + ", addressCity=" + this.getAddressCity()
                + ", phoneNumber=" + this.getPhoneNumber()
                + ", email=" + this.getEmail()
                + ", comments=" + this.getComments()
                
                + ", prospectionDate=" + this.prospectionDate 
                + ", interested=" + this.interested 
                + '}';
    }
}
