package fr.afpa.pompey.cda02.ecf01.reverso.swing;

public enum Action {
    CREATE,
    UPDATE,
    DELETE,
    LIST
}
