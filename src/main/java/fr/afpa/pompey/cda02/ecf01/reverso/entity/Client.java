package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;


public class Client extends Company {
    /*
     * The default values helps with the turnover/headcount ratio.
     * That way, clients are created with a valid set of values and the charge
     * of providing correct  lands on the class' user
     */
    private int turnover = 10;
    private int headcount = 1;
    
    /**
     * Check the ratio between Turnover and Headcount is strictly 
     * superior to 10.
     * 
     * @param turnover
     * @param headcount
     * @return 
     */
    protected static boolean turnoverHeadcountRatioIsValid(int turnover, int headcount) {
        return (headcount > 0) && (turnover / headcount > 10);
    }
    
    /**
     * Construct an empty Client, turnover and headcount default to 
     * 10 and 1, respectively.
     */
    public Client() {
        super();
    }

    /**
     * Every value is required except for the comments
     * 
     * @param businessName
     * @param domain
     * @param addressStreetNumber
     * @param addressStreetName
     * @param addressZipCode
     * @param addressCity
     * @param phoneNumber
     * @param email
     * @param comments
     * @param turnover
     * @param headcount
     * @throws ReversoException 
     */
    public Client(
        final String businessName, 
        final Domain domain, 
        final int addressStreetNumber, 
        final String addressStreetName, 
        final String addressZipCode, 
        final String addressCity, 
        final String phoneNumber, 
        final String email, 
        final String comments,
        final int turnover,
        final int headcount
    ) throws ReversoException 
    {
        super(
            businessName,
            domain,
            addressStreetNumber,
            addressStreetName,
            addressZipCode,
            addressCity,
            phoneNumber,
            email,
            comments
        );
        
        this.setTurnover(turnover);
        this.setHeadcount(headcount);
    }
    
    public int getTurnover() {
        return this.turnover;
    }

    public int getHeadcount() {
        return this.headcount;
    }

    /**
     * The headcount must be superior to zero and the ratio between 
     * turnover and headcount should be superior to 10.
     * 
     * You might want to increase this value before you set a higher headcount.
     * 
     * @param turnover
     * @throws ReversoException 
     */
    public void setTurnover(int turnover) throws ReversoException {
        if (turnover < 0) {
            throw new ReversoException(
                "Turnover must be superior to zero (received '" + turnover + "')"
            );
        }
        
        if (! this.turnoverHeadcountRatioIsValid(turnover, this.headcount)) {
            throw new ReversoException(
                "The ratio between turnover and headcount should be superiori to 10"
            );
        }
        
        this.turnover = turnover;
    }

    /**
     * The headcount must be superior to zero and the ratio between 
     * turnover and headcount should be superior to 10.
     * 
     * You might want to increase the value of turnover before you set 
     * this one higher than it is.
     * 
     * @param headcount
     * @throws ReversoException 
     */
    public void setHeadcount(int headcount) throws ReversoException {
        if (headcount < 1) {
            throw new ReversoException(
                "Headcount must be superior to zero (received '" + headcount + "')"
            );
        }

        if (! this.turnoverHeadcountRatioIsValid(this.turnover, headcount)) {
            throw new ReversoException(
                "The ratio between turnover and headcount should be superior to 10"
            );
        }

        this.headcount = headcount;
    }

    @Override
    public String toString() {
        return "Client{" 
                + "id=" + this.getId() 
                + ", businessName=" + this.getBusinessName() 
                + ", domain=" + this.getDomain()
                + ", addressStreetNumber=" + this.getAddressStreetNumber()
                + ", addressStreetName=" + this.getAddressZipCode()
                + ", addressZipCode=" + this.getAddressZipCode()
                + ", addressCity=" + this.getAddressCity()
                + ", phoneNumber=" + this.getPhoneNumber()
                + ", email=" + this.getEmail()
                + ", comments=" + this.getComments()
                
                + ", turnover=" + turnover 
                + ", headcount=" + headcount 
                + '}';
    }
}
