package fr.afpa.pompey.cda02.ecf01.reverso;


public class ReversoException extends Exception {

    public ReversoException(String message) {
        super(message);
    }
}
