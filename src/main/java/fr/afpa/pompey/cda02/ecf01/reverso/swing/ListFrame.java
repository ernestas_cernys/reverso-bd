package fr.afpa.pompey.cda02.ecf01.reverso.swing;


import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import fr.afpa.pompey.cda02.ecf01.reverso.Reverso;
import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;


/**
 * A frame containing a Table overview of Companies
 */
public class ListFrame extends javax.swing.JFrame {
    class ListTableModel extends AbstractTableModel {
        private List<Company> list;
        
        private Object[][] rowData;
        private String[] columnNames;
        
        public ListTableModel(List<Company> list, CompanyType companyType) {
            switch (companyType) {
                case CLIENT:
                    this.columnNames = new String[]{
                        "Id",
                        "Raison Sociale",
                        "Domaine",
                        "Numéro de rue",
                        "Nom de rue",
                        "Code Postal",
                        "Ville",
                        "Numéro de téléphone",
                        "Adresse mail",
                        "Commentaires",
                        "Chiffre d'affaire",
                        "Effectif"
                    };
                    break;
                
                case PROSPECT:
                    this.columnNames = new String[]{
                        "Id",
                        "Raison Sociale",
                        "Domaine",
                        "Numéro de rue",
                        "Nom de rue",
                        "Code Postal",
                        "Ville",
                        "Numéro de téléphone",
                        "Adresse mail",
                        "Commentaires",
                        "Date de prospection",
                        "Est intéréssé"
                    };
                    break;
            }
            
            this.list = list;
            this.rowData = new Object[this.getList().size()][getColumnCount()];

            for (int i = 0; i < this.getList().size(); i++) {
                Company company = this.getList().get(i);
                
                this.rowData[i][0] = company.getId();
                this.rowData[i][1] = company.getBusinessName();
                this.rowData[i][2] = company.getDomain().getRepresentation();
                this.rowData[i][3] = company.getAddressStreetNumber();
                this.rowData[i][4] = company.getAddressStreetName();
                this.rowData[i][5] = company.getAddressZipCode();
                this.rowData[i][6] = company.getAddressCity();
                this.rowData[i][7] = company.getPhoneNumber();
                this.rowData[i][8] = company.getEmail();
                this.rowData[i][9] = company.getComments();
                
                
                if (company instanceof Client) {
                    this.rowData[i][10] = ((Client) company).getTurnover();
                    this.rowData[i][11] = ((Client) company).getHeadcount();
                }
                
                if (company instanceof Prospect) {
                    this.rowData[i][10] = ((Prospect) company).getProspectionDate();
                    this.rowData[i][11] = ((Prospect) company).isInterested();
                }
            }
        }

        public List<Company> getList() {
            return list;
        }
        
        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }
        
        @Override
        public int getRowCount() { return this.rowData.length; }
        
        @Override
        public int getColumnCount() { return columnNames.length; }
        
        @Override
        public Object getValueAt(int row, int col) {
            return this.rowData[row][col];
        }
        
        @Override
        public boolean isCellEditable(int row, int col)
        { 
            return false; 
        }
        
        @Override
        public void setValueAt(Object value, int row, int col) {
            this.rowData[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }
    
    private List<Company> list;
    private CompanyType companyType;
    
    public ListFrame(
            List<Company> list, 
            CompanyType companyType
    ) {
        this.setList(list);
        this.setCompanyType(companyType);
        
        // The JTable will be populated with an instance of the ListTableModel
        // initialized with the list and companyType
        // See the code of the initComponents method
        initComponents();
    }

    public List<Company> getList() {
        return list;
    }

    public void setList(List<Company> list) {
        this.list = list;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new javax.swing.JScrollPane();
    listTable = new javax.swing.JTable();
    exitButton = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    listTable.setModel(new ListTableModel(getList(), getCompanyType()));
    listTable.setToolTipText("");
    jScrollPane1.setViewportView(listTable);

    exitButton.setText("Retour");
    exitButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 678, Short.MAX_VALUE)
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(exitButton)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
        .addContainerGap(13, Short.MAX_VALUE)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(exitButton)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        new HomeFrame().setVisible(true);
        
        this.dispose();
    }//GEN-LAST:event_exitButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws ReversoException {
        Reverso.loadTestData();
        List<Company> companies = Company.getList();

        /* Create and display the form */
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ListFrame(companies, CompanyType.CLIENT).setVisible(true);
            }
        });
    }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton exitButton;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable listTable;
  // End of variables declaration//GEN-END:variables
}
