package fr.afpa.pompey.cda02.ecf01.reverso.swing;


import java.util.List;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JButton;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;


public class HomeFrame extends javax.swing.JFrame {
    private CompanyType selectedCompanyType = null;
    private Action selectedAction = null;
    
    /**
     * Creates new form HomeFrame
     */
    public HomeFrame() {
        initComponents();
        
        this.actionSelectionPanel.setVisible(false);
        this.itemSelectionPanel.setVisible(false);
        this.itemSelectionComboBox.setRenderer(
            // Set a representation for each
            // If no item, display " - "
            (jlist, value, index, isSelected, hasFocus) -> {
                String representation = " - ";
                if (value != null) {
                    representation = ((Company)value).getBusinessName();
                }
                return new JLabel(representation);
            });
    }

    public CompanyType getSelectedCompanyType() {
        return selectedCompanyType;
    }

    public Action getSelectedAction() {
        return selectedAction;
    }
        
    /**
     * Set the internal selected company type
     * Update the selection item combo box, and add a visual hint 
     * of the selected type
     * 
     * @param type 
     */
    private void setSelectedCompanyType(CompanyType type) {
        this.selectedCompanyType = type;
        
        if (type != null) {
            this.actionSelectionPanel.setVisible(true);
            this.updateItemSelectionList();
        }
        
        // Selection hint
        for (Component component: this.objectSelectionPanel.getComponents()) {
            if (component instanceof JButton) {
                this.toggleSelectionHint((JButton) component, false);
            }
        }
        
        switch (this.getSelectedCompanyType()) {
            case CLIENT:
                this.toggleSelectionHint(this.choseClientTypeButton, true);
                break;
            case PROSPECT:
                this.toggleSelectionHint(this.choseProspectTypeButton, true);
                break;
        }
    }
    
    /**
     * Set the action, update the panels and add a visual hint 
     * on the selected action
     * 
     * @param action 
     */
    private void setSelectedAction(Action action) {
        this.selectedAction = action;
        
        // Internal state management
        switch (this.getSelectedAction()) {
            case LIST:
                List<Company> list = this.getCompanyListOf(
                        this.getSelectedCompanyType().getEquivalentClass()
                );
                ListFrame listFrame = new ListFrame(
                        list, 
                        this.getSelectedCompanyType()
                );
                
                this        .setVisible(false);
                listFrame   .setVisible(true);
                break;
                
            case CREATE:
                Company newCompany = null;
                switch (this.getSelectedCompanyType()) {
                    case CLIENT:
                        newCompany = new Client();
                        break;
                    case PROSPECT:
                        newCompany = new Prospect();
                        break;
                }
                
                this.showDetailsFrame(
                        this.getSelectedAction(), newCompany
                );
                break;
                
            case UPDATE:
            case DELETE:                
                this.itemSelectionPanel.setVisible(true);
                this.updateItemSelectionList();
                break;

            default:
                this.itemSelectionPanel.setVisible(false);
        }
        
        // Selection hint
        for (Component component: this.actionSelectionPanel.getComponents()) {
            if (component instanceof JButton) {
                this.toggleSelectionHint((JButton) component, false);
            }
        }
        
        switch (this.getSelectedAction()) {
            case CREATE:
                this.toggleSelectionHint(this.createActionButton, true);
                break;
            case UPDATE:
                this.toggleSelectionHint(this.updateActionButton, true);
                break;
            case DELETE:
                this.toggleSelectionHint(this.deleteActionButton, true);
                break;
        }
    }
    
    /**
     * Return the list of company filtered according to the given
     * argument
     * 
     * @param companyClass
     * @return 
     */
    private List<Company> getCompanyListOf(Class companyClass) {
        List<Company> resultList = new ArrayList<Company>();
        
        for (Company company: Company.getList()) {
            if (companyClass.isInstance(company)) {
                resultList.add(company);
            }
        }
        
        return resultList;
    }
    
    /**
     * Return the list of companies matching with the provided CompanyType
     * 
     * @param Type
     * @return 
     */
    private List<Company> getCompanyFilteredByType(CompanyType type) {
        List<Company> filteredList = new ArrayList<>();
        
        for (Company company: Company.getList()) {
            if (type == CompanyType.CLIENT 
                && company instanceof Client) {

                filteredList.add(company);
            }
            if (type == CompanyType.PROSPECT 
                && company instanceof Prospect) {

                filteredList.add(company);
            }
        }
        
        return filteredList;
    }
    /**
     * Update the list of item in the dedicated panel. 
     * If there is no items to display, disable the combobox and associated 
     * validation button
     */
    private void updateItemSelectionList() {
        List<Company> companyList = 
                getCompanyFilteredByType(this.selectedCompanyType);
        
        if (companyList.size() > 0) {
            this.itemSelectionComboBox          .setEnabled(true);
            this.itemSelectionValidationButton  .setEnabled(true);

            this.itemSelectionComboBox.removeAllItems();

            for (Company company: companyList) {
                this.itemSelectionComboBox.addItem(company);
            }
        }
        else {
            this.itemSelectionComboBox          .setEnabled(false);
            this.itemSelectionValidationButton  .setEnabled(false);
        }
    }
    
    /**
     * Add or remove a visual hint of selection on the given button
     * 
     * @param button
     * @param selected 
     */
    private void toggleSelectionHint(JButton button, boolean selected) {
        String prefix = "> ";
        
        if (selected) {
            button.setText(prefix + button.getText());
        }
        else if (button.getText().startsWith(prefix)) {
            button.setText(button.getText().replace(prefix, ""));
        }
    }
    
    /**
     * Prompt the company form
     * 
     * @param action
     * @param company 
     */
    private void showDetailsFrame(Action action, Company company) {
        try {
            CompanyDetailsFrame detailsFrame = new CompanyDetailsFrame(
                action,
                company
            );
            this        .setVisible(false);
            detailsFrame.setVisible(true);
        }
        catch (ReversoException re) {
            Logger.getLogger(this.getClass().getName())
                .severe(
                    "Something went wrong when instanciating" 
                    + " the CompanyDetailsFrame: " 
                    + re.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    objectSelectionPanel = new javax.swing.JPanel();
    choseClientTypeButton = new javax.swing.JButton();
    choseProspectTypeButton = new javax.swing.JButton();
    actionSelectionPanel = new javax.swing.JPanel();
    updateActionButton = new javax.swing.JButton();
    deleteActionButton = new javax.swing.JButton();
    listActionButton = new javax.swing.JButton();
    createActionButton = new javax.swing.JButton();
    itemSelectionPanel = new javax.swing.JPanel();
    itemSelectionValidationButton = new javax.swing.JButton();
    itemSelectionComboBox = new javax.swing.JComboBox<>();
    quitButton = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    objectSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Type d'objet"));
    objectSelectionPanel.setPreferredSize(new java.awt.Dimension(1096, 488));

    choseClientTypeButton.setText("Clients");
    choseClientTypeButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        choseClientTypeButtonActionPerformed(evt);
      }
    });

    choseProspectTypeButton.setText("Prospects");
    choseProspectTypeButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        choseProspectTypeButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout objectSelectionPanelLayout = new javax.swing.GroupLayout(objectSelectionPanel);
    objectSelectionPanel.setLayout(objectSelectionPanelLayout);
    objectSelectionPanelLayout.setHorizontalGroup(
      objectSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(objectSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(objectSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(choseClientTypeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(choseProspectTypeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
        .addContainerGap())
    );
    objectSelectionPanelLayout.setVerticalGroup(
      objectSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(objectSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addComponent(choseClientTypeButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(choseProspectTypeButton)
        .addContainerGap(19, Short.MAX_VALUE))
    );

    actionSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Action"));

    updateActionButton.setText("Modifier");
    updateActionButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        updateActionButtonActionPerformed(evt);
      }
    });

    deleteActionButton.setText("Supprimer");
    deleteActionButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        deleteActionButtonActionPerformed(evt);
      }
    });

    listActionButton.setText("Lister");
    listActionButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        listActionButtonActionPerformed(evt);
      }
    });

    createActionButton.setText("Créer");
    createActionButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        createActionButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout actionSelectionPanelLayout = new javax.swing.GroupLayout(actionSelectionPanel);
    actionSelectionPanel.setLayout(actionSelectionPanelLayout);
    actionSelectionPanelLayout.setHorizontalGroup(
      actionSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(actionSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(actionSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(deleteActionButton, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
          .addComponent(updateActionButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(listActionButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(createActionButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap())
    );
    actionSelectionPanelLayout.setVerticalGroup(
      actionSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(actionSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addComponent(listActionButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(createActionButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(updateActionButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(deleteActionButton)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    itemSelectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Selection de l'objet"));

    itemSelectionValidationButton.setText("Valider");
    itemSelectionValidationButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        itemSelectionValidationButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout itemSelectionPanelLayout = new javax.swing.GroupLayout(itemSelectionPanel);
    itemSelectionPanel.setLayout(itemSelectionPanelLayout);
    itemSelectionPanelLayout.setHorizontalGroup(
      itemSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(itemSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(itemSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(itemSelectionComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, itemSelectionPanelLayout.createSequentialGroup()
            .addGap(0, 125, Short.MAX_VALUE)
            .addComponent(itemSelectionValidationButton)))
        .addContainerGap())
    );
    itemSelectionPanelLayout.setVerticalGroup(
      itemSelectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(itemSelectionPanelLayout.createSequentialGroup()
        .addContainerGap()
        .addComponent(itemSelectionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(itemSelectionValidationButton)
        .addContainerGap(36, Short.MAX_VALUE))
    );

    quitButton.setText("Quitter");
    quitButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        quitButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addComponent(objectSelectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(actionSelectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(itemSelectionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addGroup(layout.createSequentialGroup()
            .addComponent(quitButton)
            .addGap(0, 0, Short.MAX_VALUE)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(itemSelectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(objectSelectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(actionSelectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
        .addComponent(quitButton)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void choseClientTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choseClientTypeButtonActionPerformed
        this.setSelectedCompanyType(CompanyType.CLIENT);
    }//GEN-LAST:event_choseClientTypeButtonActionPerformed

    private void choseProspectTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choseProspectTypeButtonActionPerformed
        this.setSelectedCompanyType(CompanyType.PROSPECT);
    }//GEN-LAST:event_choseProspectTypeButtonActionPerformed

    private void createActionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createActionButtonActionPerformed
        this.setSelectedAction(Action.CREATE);
    }//GEN-LAST:event_createActionButtonActionPerformed

    private void quitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_quitButtonActionPerformed

    private void listActionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listActionButtonActionPerformed
        this.setSelectedAction(Action.LIST);
    }//GEN-LAST:event_listActionButtonActionPerformed

    private void updateActionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateActionButtonActionPerformed
        this.setSelectedAction(Action.UPDATE);
    }//GEN-LAST:event_updateActionButtonActionPerformed

    private void deleteActionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionButtonActionPerformed
        this.setSelectedAction(Action.DELETE);
    }//GEN-LAST:event_deleteActionButtonActionPerformed

    private void itemSelectionValidationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSelectionValidationButtonActionPerformed
        this.showDetailsFrame(
            this.getSelectedAction(),
            (Company) this.itemSelectionComboBox.getSelectedItem()
        );
    }//GEN-LAST:event_itemSelectionValidationButtonActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel actionSelectionPanel;
  private javax.swing.JButton choseClientTypeButton;
  private javax.swing.JButton choseProspectTypeButton;
  private javax.swing.JButton createActionButton;
  private javax.swing.JButton deleteActionButton;
  private javax.swing.JComboBox<Company> itemSelectionComboBox;
  private javax.swing.JPanel itemSelectionPanel;
  private javax.swing.JButton itemSelectionValidationButton;
  private javax.swing.JButton listActionButton;
  private javax.swing.JPanel objectSelectionPanel;
  private javax.swing.JButton quitButton;
  private javax.swing.JButton updateActionButton;
  // End of variables declaration//GEN-END:variables
}
