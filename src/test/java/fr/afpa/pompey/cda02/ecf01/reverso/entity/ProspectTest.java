/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ecern
 */
public class ProspectTest {
  Prospect prospect = new Prospect();
  /**
   * Test of incorrect ProspectionDate value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorect_Prospection_Date_Should_Result_In_Exception(String pValue){
    assertThrows(ReversoException.class, () -> {
      prospect.setProspectionDate(pValue);},
        "Correct ProspectionDate Value is not valid :" + "\"" + pValue + "\"");
  }
  
  /**
   * Test of correct ProspectionDate value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource(strings = {"05/07/2012"})
  void correct_Prospection_Date_Should_Not_Result_In_Exception (String pValue){
    assertDoesNotThrow(() -> prospect.setProspectionDate(pValue),
      "Incorrect ProspectionDate Value : " + "\"" + pValue + "\"");
  }  
}
