package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company.Domain;
import org.junit.jupiter.params.ParameterizedTest;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;



public class CompanyTest {
  //--------------------ConcreteEntityCreation----------------------------------
  class ConcreteCompany extends Company{};
  ConcreteCompany company = new ConcreteCompany();  
  
 //-----------------------setBusinessName--------------------------------------- 
  /**
   * Test of invalid BusinessName value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_BusinessName_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setBusinessName(pValue);},
      "Correct BusinessName Value is not valid :" + "\"" + pValue + "\"");
  }
  
  /**
   * Test of valid BusinessName value in setter
   */
  @Test
  void correct_BusinessName_Value_Should_Throw_No_Exception (){
    String value = "AFPA Frouard";
    assertDoesNotThrow(()-> {company.setBusinessName(value);},
      "Incorrect BusinessName Value : \"" + value + "\"");
  }
  
  //-----------------------------setDomain--------------------------------------
  /**
   * Test of null Domain value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullSource
  void incorrect_Domain_Value_Should_Result_In_Exception (Domain pValue){
    assertThrows(ReversoException.class, ()-> {company.setDomain(pValue);},
      "Correct Domain Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of correct Domain values in setter
   * @param pValue 
   */
  @ParameterizedTest
  @EnumSource (Domain.class)
  void correct_Domain_Value_Should_Throw_No_Exception (Domain pValue){
    assertDoesNotThrow(()-> {company.setDomain(pValue);},
       "Incorrect Domain Value : \"" + pValue + "\"");
  }
  
  //-----------------------setAddressStreetNumber-------------------------------
  /**
   * Test of incorrect StreetNumber values in setter
   * @param pValue  
   */
  @ParameterizedTest
  @ValueSource (ints = {Integer.MIN_VALUE, -15, -1})
  void incorrect_StreetNumber_Input_Should_Result_In_Exception (int pValue){
      assertThrows(ReversoException.class, () -> {
        company.setAddressStreetNumber(pValue);},
          "Correct StreetNumber Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test for correct StreetNumber values in setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (ints = {1, 15, Integer.MAX_VALUE})
  void coorect_StreetNumber_Value_Should_Throw_No_Exception (int pValue){
    assertDoesNotThrow(()-> {company.setAddressStreetNumber(pValue);},
      "Incorrect StreetNumber Value : \"" + pValue + "\"");
  }
  
  //------------------------setAddressStreetName--------------------------------
  
  /**
   * Test of invalid StreetName value in setter
   * @param pValue  
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_StreetName_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setAddressStreetName(pValue);},
      "Correct StreetName Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid Street Name value in setter
   */
  @Test
  void correct_StreetName_Value_Should_Throw_No_Exception (){
    String pValue = "Avenue de l'AFPA";
    assertDoesNotThrow(()-> {company.setAddressStreetName(pValue);},
      "Incorrect StreetName Value : \"" + pValue + "\"");
  }
  
  //---------------------------setAddressZipCode--------------------------------
  /**
   * Test of invalid ZipCode value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_ZipCode_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setAddressZipCode(pValue);},
      "Correct ZipCode Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid ZipCode value in setter
   */
  @Test
  void correct_ZipCode_Value_Should_Throw_No_Exception (){
    String pValue = "67000";
    assertDoesNotThrow(()-> {company.setAddressZipCode(pValue);},
      "Incorrect ZipCode Value : \"" + pValue + "\"");
  }    
                                    
  //---------------------------setAddressCity-----------------------------------
  /**
   * Test of invalid City value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void incorrect_City_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setAddressCity(pValue);},
      "Correct City Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid City value in setter
   */
  @Test
  void correct_City_Value_Should_Throw_No_Exception (){
    String pValue = "Strasbourg";
    assertDoesNotThrow(()-> {company.setAddressCity(pValue);},
      "Incorrect City Value : \"" + pValue + "\"");                                          
  }
  
  //----------------------------setPhoneNumber----------------------------------
    /**
   * Test of invalid PhoneNumber value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {
    "",
    "   ",
    "\n",
    "\t",
    "Numéro",
    "0520357889 ",
    "88-15-45-45-45",
    "+468-45-45-46-45",
    "003506287889",
    "45455689121",
    "789456123"
  })
  void incorrect_PhoneNumber_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setPhoneNumber(pValue);},
      "Correct PhoneNumber Value is not valid : \"" + pValue + "\"");
  }
  
  
  /**
   * Test of valid PhoneNumber value in setter
   * @param pValue
   */
  @ParameterizedTest
  @ValueSource (strings = {
  "0620678945",  
  "+33606894578",
  "06.20.67.89.45",
  "+330.06.89.45.78",
  "06 20 67 89 45",
  "+330 06 89 45 78"
  })
  void correct_PhoneNumber_Value_Should_Throw_No_Exception (String pValue){
    assertDoesNotThrow(()-> {company.setPhoneNumber(pValue);},
      "Incorrect PhoneNumber Value : \"" + pValue + "\"");                                          
  }
  
  //----------------------------setEmail----------------------------------------
    /**
   * Test of invalid Email value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {
    "",
    "   ",
    "\n",
    "\t",
    "Email",
    "Planetarium@air.er.rt",
    "korona virus@smart.ltz",
    "@new.run",
    "Nevermind@tous abord",
    "neverever@",
    "@"
  })
  void incorrect_Email_Value_Should_Result_In_Exception (String pValue){
    assertThrows(ReversoException.class, ()-> {company.setEmail(pValue);},
      "Correct Email Value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of valid Email value in setter
   * @param pValue
   */
  @ParameterizedTest
  @ValueSource (strings = {
  "smart.beaver@march.fr",  
  "ide@netbeans.com",
  "me@offcourse",
  "under.world.is@real.deal.nevertheless",
  "45678@46543"
  })
  void correct_Email_Value_Should_Throw_No_Exception (String pValue){
    assertDoesNotThrow(()-> {company.setEmail(pValue);},
      "Incorrect Email Value : \"" + pValue + "\"");                                          
  }
  
  //------------------------------setComments-----------------------------------
  
   /**
   * Test of empty and null Comments value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource (strings = {"", "   ", "\n", "\t"})
  void empty_Or_Null_Comments_Value_Should_Return_Null (String pValue){
    company.setComments(pValue);
    assertNull(company.getComments(),
      "Not null or empty Comments value is not valid : \"" + pValue + "\"");
  }
  
  /**
   * Test of not empty or null Comments value in setter
   * @param pValue 
   */
  @ParameterizedTest
  @ValueSource (strings = {"a  ", "Comment", "zero\n", " h \t"})
  void not_Null_Comments_Value_Should_Return_Value (String pValue){
    company.setComments(pValue);
    assertNotNull(company.getComments(),
      "Incorrect StreetNumber Value : \"" + pValue + "\"");
  }
}
